import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Home from './pages/home';
import PantsPage from './pages/pants';
import ShirtsPage from './pages/shirts';
import ShoesPage from './pages/shoes';

function Routes() {
  return (
    <BrowserRouter>
      <Route path="/" exact component={ShoesPage} />
      <Route path="/home" exact component={Home} />
      <Route path="/shoes" exact component={ShoesPage} />
      <Route path="/shirts" exact component={ShirtsPage} />
      <Route path="/pants" exact component={PantsPage} />
    </BrowserRouter>
  );
}

export default Routes;
