/* eslint-disable react/prop-types */
/* eslint-disable import/order */
import React, { useState } from 'react';

import { FiMenu, FiSearch, FiX } from 'react-icons/fi';
import Login from './login';
import Menu from './menu';

import LogoImg from '../../assets/Logo/logo_webjump.png';
import './style.scss';
import { Link } from 'react-router-dom';

function Header({ searchFc }) {
  const [label, setLabel] = useState();
  const [menuVisible, setMenuVisible] = useState(false);

  return (
    <div id="header">
      <Login />
      <div className="top-header">
        <div className="logo">
          {!menuVisible ? (
            <FiMenu
              className="icons"
              size={30}
              color="rgba(0,0,0,0.6)"
              onClick={() => setMenuVisible(!menuVisible)}
            />
          ) : (
            <FiX
              size={30}
              className="icons"
              color="rgba(0,0,0,0.6)"
              onClick={() => setMenuVisible(!menuVisible)}
            />
          )}

          <img src={LogoImg} alt="Web Jump" />
          <FiSearch size={30} className="icons" color="#cc0d1f" />
        </div>
        <div className="search">
          <input type="text" onChange={(e) => setLabel(e.target.value)} />
          <button type="submit" onClick={() => searchFc(label)}>
            BUSCAR
          </button>
        </div>
        {menuVisible && (
          <div className="menu-hidden">
            <Link to="/home" className="link">
              <p>PÁGINA INICIAL</p>
            </Link>

            <Link to="/shirts" className="link">
              <p>CAMISETAS</p>
            </Link>

            <Link to="/pants" className="link">
              <p>CALÇAS</p>
            </Link>

            <Link to="/shoes" className="link">
              <p>SAPATOS</p>
            </Link>
          </div>
        )}
      </div>
      <Menu />
    </div>
  );
}

export default Header;
